implementation module AsyncIO

import StdEnv

import Control.Applicative
import qualified Data.Array
import Data.Error
import qualified Data.Foldable
import Data.Functor
import Data.GenEq
import qualified Data.List
import qualified Data.Map
import Data.Maybe
import qualified _AsyncIO
from Data.Func import $
from Data.Map import :: Map
from Data.Error import :: MaybeError (..), :: MaybeErrorString, isError, fromOk, fromError, instance Functor (MaybeError e)
                     , liftError
import Network.IP
from System.OSError import :: MaybeOSError, :: OSError, :: OSErrorMessage, :: OSErrorCode, getLastOSError
from System.OS import IF_WINDOWS
import System.SysCall
import System._Finalized
from _AsyncIO import :: OS (..), :: ConnectionType (..), :: FD, instance == OS, asyncIoFdToFinalizer
from Data.Foldable import class Foldable, instance Foldable []
import Control.Monad

:: IPAddr       =: IPAddr String
:: AsyncIOFD    :== Int

/**
 * Data type used to represent events returned by the I/O multiplexer.
 *
 * AcceptEventSock : Incoming connection for TCP listener.
 * ConnectEventSock: Connection attempt succeeded for TCP client.
 * ReadEventSock: Data arrived.
 * WriteEventSock: All pending data was sent.
 * NopEventSock: Nothing should be done, e.g Data might have been sent but there is still data pending or an I/O request may have been cancelled.
 * ReadAndWriteEventSock: Data was sent and received.
 * DisconnectEventSock: Disconnect was detected.
 */
:: IOEvent = AcceptEventSock | ConnectEventSock | ReadEventSock | WriteEventSock
           | NopEventSock | ReadAndWriteEventSock | DisconnectEventSock

/**
 * Pair of a file descriptor and the IOEvent that occurred on the file descriptor.
 */
:: ConnectionEvent =
	{ cId :: !ConnectionId
	, ev  :: !IOEvent
	}

emptyAIOState :: !*World -> (!MaybeOSError (*AIOState *env), !*World)
emptyAIOState world
	# (mbAioFd, world) = ioInit world
	| isError mbAioFd = (Error (fromError mbAioFd), world)
	# aioFd = fromOk mbAioFd
	= (Ok
	     { AIOState
		 | aioFd     = asyncIoFdToFinalizer aioFd
		 , listeners = 'Data.Map'.newMap
		 , clients   = 'Data.Map'.newMap
		 , writeQueues   = 'Data.Map'.newMap
		 }
		 , world)

finalizerToAioFd :: !Finalizer -> Int
finalizerToAioFd finalizer = fst $ withFinalizedInt id finalizer

addListener :: !(ConnectionHandlers *env) !Port !*env -> (!MaybeOSError ConnectionId, !*env) | containsAIOState env
addListener handlers port env
	# (aioState=:{AIOState|listeners, clients, aioFd}, env) = getAIOState env
	# (mbListenId, env) = createTCPListener (finalizerToAioFd aioFd) port env
	| isError mbListenId = (Error (fromError mbListenId), env)
	# listenId = fromOk mbListenId
	# (mbErr, env) = IF_WINDOWS (windowsAccept (finalizerToAioFd aioFd) listenId env) (Ok (Posix, ConnectionId -1),env)
	| isError mbErr = (Error (fromError mbErr), env)
	# (os, clientId) = fromOk mbErr
	| os == Posix
		# listeners = 'Data.Map'.put listenId
			{Listener
			|handlers = handlers
			, removeOnClose = True
			, mbLastClient = ?None
			} listeners
		# aioState = {AIOState|aioState & listeners = listeners}
		= (Ok (listenId), updAIOState aioState env)
    // os == Windows, monitor client.
	# listeners
		= 'Data.Map'.put listenId
		{ Listener
		| handlers = handlers
		, removeOnClose = True
		, mbLastClient = ?Just clientId
		} listeners
	# clients
		= 'Data.Map'.put clientId
		{ Client
		| handlers = handlers
		, mbIpAddr = ?None
		, connected = False
		, closeAfterWrite = False
		, evaluateHandlers = True
		, removeOnClose = True
		} clients
	# aioState = {AIOState|aioState & listeners = listeners, clients = clients}
	= (Ok listenId, updAIOState aioState env)

addConnection :: !(ConnectionHandlers *env) !Hostname !Port !*env
              -> (!MaybeOSError ConnectionId, !*env) | containsAIOState env
addConnection handlers hostaddr port env
	# (mbIp, env) = lookupIPv4Address hostaddr env
	| isNone mbIp = (Error (-1,"Failed to connect to " +++ hostaddr +++ ", lookup failed"), env)
	# (?Just ip) = mbIp
	# (aioState=:{AIOState|aioFd, clients}, env) = getAIOState env
	# (mbClient, env) = connect (finalizerToAioFd aioFd) (toInt ip) port env
	| isError mbClient = (liftError mbClient, env)
	# clientId = fromOk mbClient
	// Client is not connected at this point in time.
	# clients = 'Data.Map'.put clientId
		{Client
		| handlers = handlers
		, mbIpAddr = ?Just (IPAddr (toString ip))
		, connected = False
		, closeAfterWrite = False
		, evaluateHandlers = True
		, removeOnClose = True
		}
		clients
	# aioState = {aioState & clients = clients}
	= (Ok clientId, updAIOState aioState env)

defaultLoop :: !(?Int) !*env -> *env | containsAIOState env
defaultLoop terminationBehavior env = loop terminationBehavior ?None 10 env

loop :: !(?Int) !(?TimeoutMS) !Int !*env -> *env | containsAIOState env
loop mbTicksLeft timeoutMs maxEvents env
	| mbTicksLeft =: (?Just 0) = env
	# (aioState=:{AIOState|aioFd, writeQueues},env) = getAIOState env
	# aioState = {aioState & writeQueues = 'Data.Map'.newMap}
	# env = updAIOState aioState env
	# env = tick timeoutMs maxEvents id env
	= loop (fmap (\tl -> tl - 1) mbTicksLeft) timeoutMs maxEvents env

tick :: !(?TimeoutMS) !MaxEvents !(*env -> *env) !*env -> *env | containsAIOState env
tick timeout maxEvents updatedEnv env
	# env = processIdle env
	# fdArr = createArray maxEvents -1
	# evKindsArr = createArray maxEvents -1
	# (aioState=:{AIOState|aioFd},env) = getAIOState env
	# (mbNEvents, env) = ioGetEvents (finalizerToAioFd aioFd) timeout maxEvents fdArr evKindsArr env
	| isError mbNEvents
		# (errCode, errMsg) = fromError mbNEvents
		= abort $ 'Data.Array'.concatArr5 "Error in ioGetEvents, error code: " (toString errCode) ", error message: " errMsg "."
	# nEvents = fromOk mbNEvents
	= ioProcessEvents nEvents fdArr evKindsArr $ updatedEnv env

closeAllConnections :: !*env -> *env | containsAIOState env
closeAllConnections env
	# ({AIOState|clients, listeners}, env) = getAIOState env
	# env = 'Data.Map'.foldlWithKey` (\env cId _ -> closeListener cId env) env listeners
	= 'Data.Map'.foldlWithKey` (\env cId _  -> cleanupSocket False cId env) env clients

processIdle :: !*env -> *env | containsAIOState env
processIdle env
	= processClients env

ioProcessEvents :: !Int !{#Int} !{#Int} !*env -> *env | containsAIOState env
ioProcessEvents nEvents fdArr evArr env
	# fds = [fd \\ fd <-: 'Data.Array'.takeArr nEvents fdArr]
	# evs = [ev \\ ev <-: 'Data.Array'.takeArr nEvents evArr]
	# connectionEvents = toConnectionEvents fds (toEvents evs)
	= processIO connectionEvents env
where
	toConnectionEvents :: ![FD] ![IOEvent] -> [ConnectionEvent]
	toConnectionEvents [fd:fds] [e:es] = [{cId=(ConnectionId fd), ev=e}:(toConnectionEvents fds es)]
	toConnectionEvents [] [] = []
	toConnectionEvents x y =
		abort $
			'Data.Array'.concatArr3
				"AsyncIO.icl: Error in toConnectionEvents, lengths do not match: "
				(toString $ length x)
				(toString $ length y)

processIO :: ![ConnectionEvent] !*env -> *env | containsAIOState env
processIO [] env = env
processIO [connectionEv=:{cId,ev}:connectionEvs] env
	# ({AIOState|clients, listeners}, env) = getAIOState env
	| isNone ('Data.Map'.get cId listeners) && isNone ('Data.Map'.get cId clients) = processIO connectionEvs env
	# env = processEvent connectionEv env
	= processIO connectionEvs env

processEvent :: !ConnectionEvent !*env -> *env | containsAIOState env
processEvent {cId, ev=AcceptEventSock} env
	# (aioState=:{AIOState|listeners, writeQueues}, env) = getAIOState env
	# listener=:{Listener|handlers={ConnectionHandlers|onConnect}} = 'Data.Map'.find cId listeners
	# (mbErr, env) = acceptConnection cId env
	| isError mbErr
		# err = fromError mbErr
		# errCode = fst err
		# conReqWasAborted = errCode == -2
		// If a connection request was aborted then nothing should be done.
		= if conReqWasAborted env (evaluateOnErrorHandler cId err env)
	# (clientId, ip) = fromOk mbErr
	# (out, close, env) = onConnect clientId (IPAddr (toString o toDottedDecimal $ ip)) env
	# env = writeData clientId out env
	| close = closeConnection clientId env
	= env
where
	toDottedDecimal :: !Int -> String
	toDottedDecimal ip = 'Data.Array'.concatArr
		[ toString ((ip>>24) bitand 255), "."
		, toString ((ip>>16) bitand 255), "."
		, toString ((ip>> 8) bitand 255), "."
		, toString ( ip      bitand 255)
		]

processEvent {cId, ev=ConnectEventSock} env
	# (mbDisconnected, env) = IF_WINDOWS (windowsReadSock cId env) (Ok False, env)
	| isError mbDisconnected
		# env = evaluateOnErrorHandler cId (fromError mbDisconnected) env
		= cleanupSocket True cId env
	# disconnected = fromOk mbDisconnected
	| disconnected = cleanupSocket True cId env
	# (aioState=:{AIOState|clients}, env) = getAIOState env
	| isNone ('Data.Map'.get cId clients) = abort "ConnectEventSock, client not found"
	# client=:{Client|mbIpAddr, handlers={ConnectionHandlers|onConnect}} = 'Data.Map'.find cId clients
	# (out, close, env) = onConnect cId (fromJust mbIpAddr) env
	# env = writeData cId out env
	# (aioState=:{AIOState|clients}, env) = getAIOState env
	# clients = 'Data.Map'.put cId {client & connected = True} clients
	# aioState = {aioState & clients = clients}
	# env = updAIOState aioState env
	| close = closeConnection cId env
	= env

processEvent readEv=:{cId, ev=ReadEventSock} env
	# ({AIOState|aioFd, writeQueues}, env) = getAIOState env
	# (closed, env) = isConnectionBeingClosed cId env
	| closed = env
	# (mbErr, env) = retrieveData (finalizerToAioFd aioFd) cId env
	| isError mbErr
		# error=:(errCode, errMsg) = fromError mbErr
		# receiveBufferIsEmpty = IF_WINDOWS False (errCode == 11 || errCode == 35) // EAGAIN OR EWOULDBLOCK (POSIX)
		| not receiveBufferIsEmpty
			# env = evaluateOnErrorHandler cId error env
			= cleanupSocket True cId env
		= env
	# (disconnected, data) = fromOk mbErr
	| disconnected
		# env = IF_WINDOWS (processData data env) env
		= cleanupSocket True cId env
	# (mbDisconnected, env) = IF_WINDOWS (windowsReadSock cId env) (Ok False, env)
	# env = processData data env
	| isError mbDisconnected
		# env = evaluateOnErrorHandler cId (fromError mbDisconnected) env
		= cleanupSocket True cId env
	# disconnected = fromOk mbDisconnected
	| disconnected = cleanupSocket True cId env
	= IF_WINDOWS env (processEvent readEv env)
where
	processData data env
		# (aioState=:{AIOState|writeQueues, clients}, env) = getAIOState env
		| isNone ('Data.Map'.get cId clients) = abort "processData: could not find client."
		# {Client|handlers={ConnectionHandlers|onData}} = 'Data.Map'.find cId clients
		# (out, close, env) = onData cId data env
		# env = writeData cId out env
		| close = closeConnection cId env
		= env

processEvent {cId, ev=WriteEventSock} env
	# ({AIOState|clients}, env) = getAIOState env
	# mbClient = 'Data.Map'.get cId clients
	// Can happen that a write event occurs for a client that has been disconnected in the meantime.
	| isNone mbClient = env
	# {Client|closeAfterWrite} = fromJust mbClient
	| closeAfterWrite = cleanupSocket False cId env
	= env

// Data has been attempted to be sent but there is still data remaining to be sent.
processEvent {cId, ev=NopEventSock} env
	= env

// This is a Linux only event. Could go wrong since socket may be both readable and writable yet WriteEventNop should be returned/processed.
processEvent {cId, ev=ReadAndWriteEventSock} env
	# env = processEvent {ConnectionEvent|cId = cId, ev = ReadEventSock} env
	= processEvent {ConnectionEvent|cId = cId, ev = WriteEventSock} env

processEvent {cId, ev=DisconnectEventSock} env
	// On Linux on a disconnect there can still be data in the read buffer, which should be read.
	# env = IF_WINDOWS env (processEvent {ConnectionEvent|cId = cId, ev = ReadEventSock} env)
	= cleanupSocket True cId env

writeData :: !ConnectionId ![OutputData] !*env -> *env | containsAIOState env
writeData cId out env
	# out = filter ((<>) "") out
	# (aioState=:{AIOState|aioFd}, env) = getAIOState env
	// Signals data should be written on this connection to the internal I/O multiplexing mechanism.
	# (mbErr, env) = if (out <> []) (signalWriteSock (finalizerToAioFd aioFd) cId env) (Ok (), env)
	| isError mbErr = evaluateOnErrorHandler cId (fromError mbErr) env
	# (mbErr, env) = IF_WINDOWS (windowsIncPacketsToWrite cId (length out) env) (Ok (), env)
	| isError mbErr = evaluateOnErrorHandler cId (fromError mbErr) env
	= addDataToQueue out env
where
	addDataToQueue :: ![String] !*env -> *env | containsAIOState env
	addDataToQueue [msg:msgs] env
		# (aioState=:{AIOState|aioFd}, env) = getAIOState env
		# (mbDisconnected, env) = queueWriteSock (finalizerToAioFd aioFd) cId msg env
		| isError mbDisconnected = evaluateOnErrorHandler cId (fromError mbDisconnected) env
		# disconnected = fromOk mbDisconnected
		| disconnected = cleanupSocket True cId env
		= addDataToQueue msgs env
	addDataToQueue [] env = env

addToWriteMap :: ![String] !(?(![String], !Bool)) -> (?(![String], !Bool))
addToWriteMap [] x = x
addToWriteMap toAdd ?None = ?Just (toAdd, False)
addToWriteMap toAdd (?Just (existing, closed)) = ?Just (existing ++ toAdd, closed)

toEvents :: ![Int] -> [IOEvent]
toEvents evs = 'Data.List'.map toEvent evs
where
	toEvent 0  = AcceptEventSock
	toEvent 1  = ConnectEventSock
	toEvent 2  = ReadEventSock
	toEvent 4  = WriteEventSock
	toEvent 6  = ReadAndWriteEventSock
	toEvent 8  = DisconnectEventSock
	toEvent 10 = NopEventSock

isConnectionBeingClosed :: !ConnectionId !*env -> (!Bool, !*env) | containsAIOState env
isConnectionBeingClosed cId env
	# ({AIOState|clients, writeQueues}, env) = getAIOState env
	# mbClient = 'Data.Map'.get cId clients
	| isNone mbClient = (True, env)
	# {Client|closeAfterWrite, evaluateHandlers} = fromJust mbClient
	= (closeAfterWrite || not evaluateHandlers, env)

closeListener :: !ConnectionId  !*env -> *env | containsAIOState env
closeListener lId env
	# (aioState=:{AIOState|listeners, aioFd}, env) = getAIOState env
	# (mbErr, env) = cleanupFd (finalizerToAioFd aioFd) lId Socket env
	| isError mbErr = evaluateOnErrorHandler lId (fromError mbErr) env
	# aioState = {aioState & listeners = 'Data.Map'.del lId listeners}
	= updAIOState aioState env

closeConnection :: !ConnectionId !*env -> *env | containsAIOState env
closeConnection cId env
	# (aioState=:{AIOState|clients}, env) = getAIOState env
	# alreadyCleanedUp = not ('Data.Map'.member cId clients)
	| alreadyCleanedUp = env
	// Closing the connection needs to be idempotent as errors lead to attempting to close the connection.
	// And errors may occur while closing the connection as well.
	# alreadyClosing = not $ maybe False (\{evaluateHandlers} -> evaluateHandlers) $ 'Data.Map'.get cId clients
	| alreadyClosing = env
	// evaluateHandlers is set to false in the state to ensure idempotency.
	# clients
		= 'Data.Map'.alter (\mbClient -> mbClient >>= \client -> pure {client & evaluateHandlers = False}) cId clients
	# aioState & clients = clients
	# env = updAIOState aioState env
	# (mbAnyPendingPackets, env) = anyPendingPackets cId env
	| isError mbAnyPendingPackets = evaluateOnErrorHandler cId (fromError mbAnyPendingPackets) env
	# anyPendingPackets = fromOk mbAnyPendingPackets
	// If no more packets are sent the socket can immidiately be cleaned up.
	| not anyPendingPackets = cleanupSocket False cId env
	// Otherwise the remaining data has to be sent. WriteEvent (which signals that all data has been sent)
	// Calls cleanupSocket immidately.
	= env

cleanupSocket :: !Bool !ConnectionId !*env -> *env | containsAIOState env
cleanupSocket closedByPeer cId env
	# (aioState=:{AIOState|clients, aioFd}, env) = getAIOState env
	// Disconnect being detected by ioGetEvents may lead to a read which may detect a disconnect as well.
	# alreadyCleanedUp = not ('Data.Map'.member cId clients)
	| alreadyCleanedUp = env
	# {Client|handlers={ConnectionHandlers|onDisconnect}} = 'Data.Map'.find cId clients
	# (mbErr, env) = cleanupFd (finalizerToAioFd aioFd) cId Socket env
	# clients = 'Data.Map'.del cId clients
	# aioState = {aioState & clients = clients}
	# env = updAIOState aioState env
	| isError mbErr = evaluateOnErrorHandler cId (fromError mbErr) env
	= onDisconnect closedByPeer cId env

acceptConnection :: !ConnectionId
                    !*env
                    -> (!MaybeOSError (ConnectionId, Int), !*env) | containsAIOState env
acceptConnection cId env
	# (aioState=:{AIOState|aioFd, listeners, clients}, env) = getAIOState env
	# mbListener = 'Data.Map'.get cId listeners
	| isNone mbListener = (Error (-1, "AsyncIO: AcceptEventSock, no listener found."), env)
	# listener=:{Listener|handlers, removeOnClose, mbLastClient} = fromJust mbListener
	// Accept connection.
	# (mbErr,env) = accept (finalizerToAioFd aioFd) cId env
	| isError mbErr
		# (errCode, errMsg) = fromError mbErr
		// Connection request was aborted, nothing should be done.
		| errCode == -2 = (Error(-2, "Connection request was aborted."), env)
		= getLastOSError env
	#! (os, newClientId) = fromOk mbErr
	# connectedClientId = case os of
		Posix = newClientId
		Windows = fromJust mbLastClient
	// Retrieve IP addr for peer.
	# (mbErr,env) = getpeername connectedClientId cId env
	| isError mbErr = getLastOSError env
	# ip = fromOk mbErr
	// Initialize read on Windows
	# (mbDisconnected, env) = IF_WINDOWS (windowsReadSock connectedClientId env) (Ok False, env)
	| isError mbDisconnected
		# env = cleanupSocket True cId env
		= (liftError mbDisconnected, env)
	# disconnected = fromOk mbDisconnected
	| disconnected
		# env = cleanupSocket True cId env
		= (Error(-2, "Connection request was aborted."), env)
	// Deal with State.
	# client = {Client|mbIpAddr = ?None, handlers = listener.Listener.handlers, connected = True
                   , closeAfterWrite = False, evaluateHandlers = True, removeOnClose = removeOnClose}
	# clients  = 'Data.Map'.put connectedClientId client clients
	# listeners = 'Data.Map'.put cId {listener & mbLastClient = ?Just newClientId} listeners
	# aioState = {aioState & clients = clients, listeners = listeners}
	# env = updAIOState aioState env
	= (Ok (connectedClientId, ip), env)

processClients :: !*env -> *env | containsAIOState env
processClients env
	# ({AIOState|clients}, env) = getAIOState env
	= 'Data.Map'.foldrWithKey` processClient env clients
where
	processClient :: !ConnectionId (Client *env) *env -> *env | containsAIOState env
	processClient clientId client=:{Client|connected, handlers={ConnectionHandlers|onTick}} env
		| not connected = env
		# (aioState=:{aioFd}, env) = getAIOState env
		# (closed, env) = isConnectionBeingClosed clientId env
		| closed
			# (aioState=:{AIOState|clients, aioFd}, env) = getAIOState env
			# clients = 'Data.Map'.put clientId {Client|client & closeAfterWrite = True} clients
			# aioState = {aioState & clients = clients}
			= updAIOState aioState env
		# (out, close, env) = onTick clientId env
		# env = writeData clientId out env
		| close
			# env = closeConnection clientId env
			# (aioState=:{clients}, env) = getAIOState env
			# clients
				= 'Data.Map'.alter
					(\mbClient -> mbClient >>= \client -> pure {client & closeAfterWrite = True})
					clientId clients
			# aioState = {aioState & clients = clients}
			= updAIOState aioState env
		# (aioState=:{aioFd}, env) = getAIOState env
		= env

listenerEvents :: [IOEvent]
listenerEvents = [AcceptEventSock]

evaluateOnErrorHandler ::
	!ConnectionId !(!OSErrorCode, !OSErrorMessage) !*env -> *env | containsAIOState env
evaluateOnErrorHandler cId error env
	# (aioState=:{AIOState|clients}, env) = getAIOState env
	| isNone ('Data.Map'.get cId clients) = abort ("evaluateOnErrorHandler: could not find client. Error:" +++ snd error)
	# {Client|handlers={ConnectionHandlers|onError}} = 'Data.Map'.find cId clients
	= onError cId error env

ioInit :: !*env -> (!MaybeOSError AsyncIOFD, !*env) | SysCallEnv env
ioInit env = '_AsyncIO'.ioInit env

ioGetEvents :: !AsyncIOFD !(?TimeoutMS) !MaxEvents !{#Int} !{#Int} !*env -> (!MaybeOSError Int, !*env) | SysCallEnv env
ioGetEvents asyncIoFd timeout maxEvents fdList evKinds env
	= '_AsyncIO'.ioGetEvents asyncIoFd timeout maxEvents fdList evKinds env

windowsAccept :: !AsyncIOFD !ConnectionId !*env -> (!MaybeOSError (OS,ConnectionId), !*env) | SysCallEnv env
windowsAccept mainFD (ConnectionId listenFd) env
	# (mbOsClient, env) = '_AsyncIO'.windowsAccept mainFD listenFd env
	= (fmap (\(os, clientFd) -> (os, ConnectionId clientFd)) mbOsClient, env)

accept :: !AsyncIOFD !ConnectionId !*env -> (!MaybeOSError (OS,ConnectionId), !*env) | SysCallEnv env
accept mainFD (ConnectionId listenFd) env
	# (mbOsClient, env) = '_AsyncIO'.accept mainFD listenFd env
	= (fmap (\(os, clientFd) -> (os, ConnectionId clientFd)) mbOsClient, env)

createTCPListener :: !AsyncIOFD !Port !*env -> (!MaybeOSError ConnectionId,!*env) | SysCallEnv env
createTCPListener asyncIoFd port env
	# (mbListener, env) = '_AsyncIO'.createTCPListener asyncIoFd port env
	= (fmap (\listenFd -> ConnectionId listenFd) mbListener, env)

connect :: !AsyncIOFD !Int !Port !*env -> (!MaybeOSError ConnectionId, !*env) | SysCallEnv env
connect asyncIoFd ip port env
	# (mbClient, env) = '_AsyncIO'.connect asyncIoFd ip port env
	= (fmap (\clientFd -> ConnectionId clientFd) mbClient, env)

queueWriteSock :: !AsyncIOFD !ConnectionId !String !*env -> (!MaybeOSError Bool, !*env) | SysCallEnv env
queueWriteSock aioFd (ConnectionId fd) data env = '_AsyncIO'.queueWriteSock aioFd fd data env

signalWriteSock :: !AsyncIOFD !ConnectionId !*env -> (!MaybeOSError (), !*env) | SysCallEnv env
signalWriteSock asyncIoFd (ConnectionId fd) env = '_AsyncIO'.signalWriteSock asyncIoFd fd env

getpeername :: !ConnectionId !ConnectionId !*env -> (!MaybeOSError Int, !*env) | SysCallEnv env
getpeername (ConnectionId clientFd) (ConnectionId listenFd) env = '_AsyncIO'.getpeername clientFd listenFd env

retrieveData :: !AsyncIOFD !ConnectionId !*env -> (!MaybeOSError (Bool, String), !*env) | SysCallEnv env
retrieveData asyncIoFd (ConnectionId fd) env = '_AsyncIO'.retrieveData asyncIoFd fd env

windowsReadSock :: !ConnectionId !*env -> (!MaybeOSError Bool, !*env) | SysCallEnv env
windowsReadSock (ConnectionId fd) env = '_AsyncIO'.windowsReadSock fd env

cleanupFd :: !AsyncIOFD !ConnectionId !ConnectionType !*env -> (!MaybeOSError (), !*env) | SysCallEnv env
cleanupFd asyncIoFd (ConnectionId fd) cType env = '_AsyncIO'.cleanupFd asyncIoFd fd cType env

windowsIncPacketsToWrite :: !ConnectionId !Int !*env -> (!MaybeOSError (), !*env) | SysCallEnv env
windowsIncPacketsToWrite (ConnectionId fd) numPackets env = '_AsyncIO'.windowsIncPacketsToWrite fd numPackets env

anyPendingPackets :: !ConnectionId !*env -> (!MaybeOSError Bool, !*env) | SysCallEnv env
anyPendingPackets (ConnectionId fd) env = '_AsyncIO'.anyPendingPackets fd env

instance < ConnectionId where
	(<) (ConnectionId x) (ConnectionId y) = x < y

instance toString ConnectionId where
	toString (ConnectionId id) = toString id

instance == ConnectionId
where
	(==) (ConnectionId a) (ConnectionId b) = a == b

instance toString Port where
	toString (Port p) = toString p

instance < Port where
	(<) (Port p1) (Port p2) = p1 < p2

instance == Port where
	(==) (Port p1) (Port p2) = p1 == p2

instance == IOEvent
where
	(==) a b = a === b

instance == ConnectionType
where
	(==) a b = a === b

derive gEq IOEvent, ConnectionType
