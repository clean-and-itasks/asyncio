definition module _AsyncIO

from AsyncIO import :: AsyncIOFD, :: TimeoutMS, :: Port, :: MaxEvents, :: ConnectionType
from System.OSError import :: MaybeOSError (..), :: OSError (..), :: OSErrorCode, :: OSErrorMessage
from StdClass import class <, class ==
from Data.Map import :: Map
from Data.Error import :: MaybeError (..), :: MaybeErrorString
from System.SysCall import class SysCallEnv
from System._Finalized import :: Finalizer

/**
 * @representation File descriptor
 */
:: FD :== Int

:: OS = Windows | Posix

instance == OS

ioInit :: !*env -> (!MaybeOSError AsyncIOFD, !*env) | SysCallEnv env special env=World

asyncIoFdToFinalizer :: !AsyncIOFD -> Finalizer

ioGetEvents :: !AsyncIOFD !(?TimeoutMS) !MaxEvents !{#Int} !{#Int} !*env -> (!MaybeOSError Int, !*env) | SysCallEnv env special env=World

windowsAccept :: !AsyncIOFD !FD !*env -> (!MaybeOSError (OS,FD), !*env) | SysCallEnv env special env=World

accept :: !AsyncIOFD !FD !*env -> (!MaybeOSError (OS,FD), !*env) | SysCallEnv env special env=World

createTCPListener :: !AsyncIOFD !Port !*env -> (!MaybeOSError FD,!*env) | SysCallEnv env special env=World

//* The IP Address is supposed to be in network byte order
connect :: !AsyncIOFD !Int !Port !*env -> (!MaybeOSError FD, !*env) | SysCallEnv env special env=World

//* The `Bool` indicates whether the peer has disconnected.
queueWriteSock :: !AsyncIOFD !FD !String !*env -> (!MaybeOSError Bool, !*env) | SysCallEnv env special env=World

signalWriteSock :: !AsyncIOFD !FD !*env -> (!MaybeOSError (), !*env) | SysCallEnv env special env=World

getpeername :: !FD !FD !*env -> (!MaybeOSError Int, !*env) | SysCallEnv env special env=World

retrieveData :: !AsyncIOFD !FD !*env -> (!MaybeOSError (Bool, String), !*env) | SysCallEnv env special env=World

//* The `Bool` indicates whether the peer has disconnected.
windowsReadSock :: !FD !*env -> (!MaybeOSError Bool, !*env) | SysCallEnv env special env=World

cleanupFd :: !AsyncIOFD !FD !ConnectionType !*env -> (!MaybeOSError (), !*env) | SysCallEnv env special env=World

windowsIncPacketsToWrite :: !FD !Int !*env -> (!MaybeOSError (), !*env) | SysCallEnv env special env=World

anyPendingPackets :: !FD !*env -> (!MaybeOSError Bool, !*env) | SysCallEnv env special env=World
