implementation module _AsyncIO

import code from "cAsyncIO.o", "hashtable.o", library "asyncio_ws2_32_library", library "asyncio_kernel32_library"

from Data.Error import :: MaybeError (..), :: MaybeErrorString, isError, fromOk, fromError
import Data.Func
import qualified Data.List
from Data.Map import :: Map
import qualified Data.Map
import Data.Maybe
import Data.Tuple
import StdEnv
import System.SysCall
from System.OSError import :: MaybeOSError, :: OSError, :: OSErrorMessage, :: OSErrorCode, getLastOSError
from System._OSError import WSAECONNRESET
from System.OS import IF_WINDOWS
import System._Finalized

from AsyncIO import :: AsyncIOFD, :: TimeoutMS, :: Port (..), :: MaxEvents, :: ConnectionType (..), instance == ConnectionType

instance == OS
where
	(==) Posix Posix = True
	(==) Windows Windows = True
	(==) _ _ = False

ioInit :: !*env -> (!MaybeOSError AsyncIOFD, !*env) | SysCallEnv env
ioInit env
	# (asyncIoFd, env) = ioInitC env
	| asyncIoFd == -1 = getLastOSError env
	= (Ok asyncIoFd, env)
where
	ioInitC :: !*env -> (!AsyncIOFD, !*env)
	ioInitC env = code {
		ccall ioInitC ":I:A"
	}

asyncIoFdToFinalizer :: !AsyncIOFD -> Finalizer
asyncIoFdToFinalizer aioFd = finalizeInt aioFd closeAioFdPtr

closeAioFdPtr :: Pointer
closeAioFdPtr = code {
	ccall closeIocpHandlePtr ":p"
}

ioGetEvents :: !AsyncIOFD !(?TimeoutMS) !MaxEvents !{#Int} !{#Int} !*env -> (!MaybeOSError Int, !*env) | SysCallEnv env
ioGetEvents asyncIoFd timeout maxEvents fdList evKinds env
	# timeoutSettings = if (isNone timeout) (0, False) (fromJust timeout, True)
	# (numEvents, env) = ioGetEventsC asyncIoFd timeoutSettings maxEvents fdList evKinds env
	| numEvents == -1 = getLastOSError env
	= (Ok numEvents, env)
where
	ioGetEventsC :: !AsyncIOFD (!TimeoutMS,!Bool) !MaxEvents !{#Int} !{#Int} !*env -> (!Int, !*env)
	ioGetEventsC asyncIoFd (timeout,doTimeout) maxEvents fdList evKinds env = code {
		ccall ioGetEventsC "IIIIAA:I:A"
	}

windowsAccept :: !AsyncIOFD !FD !*env -> (!MaybeOSError (OS,FD), !*env) | SysCallEnv env
windowsAccept mainFD listenFd env
	# ((err, fd), env) = windowsAcceptC mainFD listenFd env
    | err == -2 = (Error (0, "Connection request was aborted"), env)
	| err <> 1 = getLastOSError env
    = (Ok (Windows, fd), env)
where
	windowsAcceptC :: !AsyncIOFD !FD !*env -> (!(!OSErrorCode, !FD), !*env)
	windowsAcceptC mainFD listenFd env = code {
		ccall windowsAcceptC "II:VII:A"
	}

accept :: !AsyncIOFD !FD !*env -> (!MaybeOSError (OS,FD), !*env) | SysCallEnv env
accept mainFD listenFd env
	# ((err, fd), env) = acceptC mainFD listenFd env
	| err == -1 = getLastOSError env
	| err == -2 = (Error (0, "Connection request was aborted"), env)
	| err == 1 = (Ok (Windows,fd), env)
	= (Ok (Posix, fd), env)
where
	acceptC :: !AsyncIOFD !FD !*env -> (!(!OSErrorCode, !FD), !*env)
	acceptC mainFD listenFd env = code {
		ccall acceptCAsyncIO "II:VII:A"
	}

createTCPListener :: !AsyncIOFD !Port !*env -> (!MaybeOSError FD,!*env) | SysCallEnv env
createTCPListener asyncIoFd (Port port) env
	# ((err, fd), env) = createTCPListenerC asyncIoFd port env
	| err == -1 = getLastOSError env
	= (Ok fd, env)
where
	createTCPListenerC :: !AsyncIOFD !Int !*env -> (!(!OSErrorCode, !FD),!*env)
	createTCPListenerC asyncIoFd port env = code {
		ccall tcplistenC "II:VII:A"
	}

connect :: !AsyncIOFD !Int !Port !*env -> (!MaybeOSError FD, !*env) | SysCallEnv env
connect asyncIoFd ip (Port port) env
	# ((err, fd), env) = connectC asyncIoFd ip port env
	| err == -1 = getLastOSError env
	= (Ok fd, env)
where
	connectC :: !AsyncIOFD Int !Int !*env -> (!(!OSErrorCode, !FD), !*env)
	connectC asyncIoFd ip port env = code {
		ccall connectC "III:VII:A"
	}

queueWriteSock :: !AsyncIOFD !FD !String !*env -> (!MaybeOSError Bool, !*env) | SysCallEnv env
queueWriteSock aioFd fd data env
	# (err, env) = queueWriteSockC aioFd fd (data) (size data) env
	| err == -1
		# ((errCode, errMsg), env) = appFst fromError $ getLastOSError env
		| errCode == WSAECONNRESET = (Ok True, env)
		= (Error (errCode, errMsg), env)
	= (Ok False, env)
where
	queueWriteSockC :: !AsyncIOFD !FD !String !Int !*env -> (!OSErrorCode, !*env)
	queueWriteSockC aioFd fd data size env = code {
		ccall queueWriteSockC "IIsI:I:A"
	}

signalWriteSock :: !AsyncIOFD !FD !*env -> (!MaybeOSError (), !*env) | SysCallEnv env
signalWriteSock asyncIoFd socket env
	# (err, env) = signalWriteSockC asyncIoFd socket env
	| err == -1 = getLastOSError env
	= (Ok (), env)
where
	signalWriteSockC :: !AsyncIOFD !FD !*env -> (!OSErrorCode, !*env)
	signalWriteSockC asyncIoFd socket env = code {
		ccall signalWriteSockC "II:I:A"
	}

getpeername :: !FD !FD !*env -> (!MaybeOSError Int, !*env) | SysCallEnv env
getpeername clientFd listenFd env
	# ((err, ip), env) = getpeernameC clientFd listenFd env
	| err == -1 = getLastOSError env
	= (Ok ip, env)
where
	getpeernameC :: !FD !FD !*env -> (!(!OSErrorCode, !Int), !*env)
	getpeernameC clientFd listenFd env = code {
		ccall getpeernameC "II:II:A"
	}

retrieveData :: !AsyncIOFD !FD !*env -> (!MaybeOSError (Bool, String), !*env) | SysCallEnv env
retrieveData asyncIoFd fd env
	# ((err, data), env) = retrieveDataC asyncIoFd fd env
	| err == -1 = getLastOSError env
	// Disconnect detected.
	| err == 0 = (Ok (True, data), env)
	= (Ok (False, data), env)
where
	retrieveDataC :: !AsyncIOFD !FD !*env -> (!(!OSErrorCode, !String), !*env)
	retrieveDataC asyncIoFd fd env = code {
		ccall retrieveDataC "II:VIS:A"
	}

windowsReadSock :: !FD !*env -> (!MaybeOSError Bool, !*env) | SysCallEnv env
windowsReadSock socket env
	# (err, env) = windowsReadSockC socket env
	| err == WSAECONNRESET = (Ok True, env)
	| err <> 0 = getLastOSError env
	= (Ok False,env)
where
	windowsReadSockC :: !FD !*env -> (!Int, !*env)
	windowsReadSockC socket env = code {
		ccall windowsReadSockC "I:VI:A"
	}

cleanupFd :: !AsyncIOFD !FD !ConnectionType !*env -> (!MaybeOSError (), !*env) | SysCallEnv env
cleanupFd asyncIoFd fd cType env
	# isASocket = cType == Socket
	# (err, env) = cleanupFdC asyncIoFd fd isASocket env
	| err == -1 = getLastOSError env
	= (Ok (), env)
where
	cleanupFdC :: !AsyncIOFD !FD !Bool !*env -> (!Int, !*env)
	cleanupFdC asyncIoFd fd isASocket env = code {
		ccall cleanupFdC "III:I:A"
	}

windowsIncPacketsToWrite :: !FD !Int !*env -> (!MaybeOSError (), !*env) | SysCallEnv env
windowsIncPacketsToWrite fd numPackets env
	# (err, env) = windowsIncPacketsToWriteC fd numPackets env
	| err == -1 = getLastOSError env
	= (Ok (), env)
where
	windowsIncPacketsToWriteC :: !FD !Int !*env -> (!Int, !*env)
	windowsIncPacketsToWriteC fd numPackets env = code {
			ccall windowsIncPacketsToWriteC "II:I:A"
		}

anyPendingPackets :: !FD !*env -> (!MaybeOSError Bool, !*env) | SysCallEnv env
anyPendingPackets fd env
	# (err, anyPendingPackets,env) = anyPendingPacketsC fd env
	| err == -1 = getLastOSError env
    = (Ok anyPendingPackets, env)
where
	anyPendingPacketsC :: !FD !*env -> (!Int, !Bool, !*env)
	anyPendingPacketsC fd env = code {
		ccall anyPendingPacketsC "I:II:A"
	}
