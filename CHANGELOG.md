# Changelog

#### 3.3.3

- Fix: using `ioInitC` resulted in a memory leak if it was evaluated multiple times
       during a programs lifetime. This is only an issue if your program repeatedly creates
	  `AIOWorld` objects.

#### 3.3.2

- Chore: accept `base` `3.0`.

#### 3.3.1

- Fix: prevent segfault by checking whether an entry in the hashtable was
  found on posix.

### 3.3.0

- Feature: expose `ConnectionId` type (i.e. make non abstract).

### 3.2.0

- Feature: export C queue header file within package for posix target.

### 3.1.0

- Feature: add `queue.freePacket`.
- Fix: solve memory leak in error conditions.

#### 3.0.3

- Chore: allow system ^2 and containers ^2.

#### 3.0.2

- Chore: include library files in nitrile.yml and rename to make unique.

#### 3.0.1

- Enhancement: copy/allocate data for packets to be enqueued within the enqueue function of queue.c

## 3.0.0

- Change: Remove `containsWorld`, this is replaced by `SysCallEnv` from the `syscall` package.
- Change: `connect` from `_AsyncIO` now requires the IP address to be in network byte order (the default toInt from `system`s `Network.IP`.
- Chore: Remove dependency on `tcpip` and suport new `base` (`>=2.0`).

## 2.0.0

- Change: add environment update function to `AsyncIO.tick`.

#### 1.0.1

- Filter out empty `String`s from output to be sent to avoid sending empty data.

## 1.0.0

- Initial version, moved asyncio from clean platform v0.3.34 to seperate package.
- Change: AsyncIO module `onDisconnect` callback now receives an argument which
          indicates whether peer disconnected or the disconnect was initiated out of own initiative.
